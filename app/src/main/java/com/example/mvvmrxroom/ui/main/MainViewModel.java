package com.example.mvvmrxroom.ui.main;

import com.example.mvvmrxroom.data.DataManager;
import com.example.mvvmrxroom.ui.base.BaseViewModel;
import com.example.mvvmrxroom.utils.rx.SchedulerProvider;

public class MainViewModel extends BaseViewModel {
    public MainViewModel(DataManager dataManager, SchedulerProvider schedulerProvider) {
        super(dataManager, schedulerProvider);
    }
}
