package com.example.mvvmrxroom.ui.main;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;

import com.example.mvvmrxroom.R;
import com.example.mvvmrxroom.ViewModelProviderFactory;
import com.example.mvvmrxroom.databinding.ActivityMainBinding;
import com.example.mvvmrxroom.ui.base.BaseActivity;
import com.example.mvvmrxroom.ui.blog.BlogFragment;

import javax.inject.Inject;

import dagger.android.AndroidInjector;
import dagger.android.DispatchingAndroidInjector;
import dagger.android.support.HasSupportFragmentInjector;

public class MainActivity extends BaseActivity<ActivityMainBinding, MainViewModel> implements HasSupportFragmentInjector, MainNavigator {

    @Inject
    ViewModelProviderFactory factory;
    @Inject
    DispatchingAndroidInjector<Fragment> fragmentDispatchingAndroidInjector;

    private MainViewModel mainViewModel;
    private ActivityMainBinding mActivityMainBinding;

    @Override
    public int getBindingVariable() {
        return com.example.mvvmrxroom.BR.viewModel;
    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_main;
    }

    @Override
    public MainViewModel getViewModel() {
        mainViewModel = ViewModelProviders.of(this, factory).get(MainViewModel.class);
        return mainViewModel;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mActivityMainBinding = getViewDataBinding();
        mainViewModel.setNavigator(this);
        setUp();

    }

    private void setUp() {
        getSupportFragmentManager().beginTransaction().add(R.id.main_container,BlogFragment.newInstance(), null).commitAllowingStateLoss();
    }

    @Override
    public AndroidInjector<Fragment> supportFragmentInjector() {
        return fragmentDispatchingAndroidInjector;
    }

    @Override
    public void handleError() {

    }
}
