package com.example.mvvmrxroom.ui.blog;

import java.util.List;

public interface BlogNavigator {

    void handleError(Throwable throwable);

    void updateBlog(List<BlogResponse.Blog> blogList);
}
