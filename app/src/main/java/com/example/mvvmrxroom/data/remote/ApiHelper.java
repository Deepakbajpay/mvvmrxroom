package com.example.mvvmrxroom.data.remote;

import com.example.mvvmrxroom.ui.blog.BlogResponse;

import io.reactivex.Single;

public interface ApiHelper {
/*

    Single<LoginResponse> doFacebookLoginApiCall(LoginRequest.FacebookLoginRequest request);

    Single<LoginResponse> doGoogleLoginApiCall(LoginRequest.GoogleLoginRequest request);

    Single<LogoutResponse> doLogoutApiCall();

    Single<LoginResponse> doServerLoginApiCall(LoginRequest.ServerLoginRequest request);
*/

    ApiHeader getApiHeader();

    Single<BlogResponse> getBlogApiCall();

//    Single<OpenSourceResponse> getOpenSourceApiCall();
}
