package com.example.mvvmrxroom.data.local.db;

import androidx.room.Database;
import androidx.room.RoomDatabase;

import com.example.mvvmrxroom.data.local.db.dao.OptionDao;
import com.example.mvvmrxroom.data.local.db.dao.QuestionDao;
import com.example.mvvmrxroom.data.local.db.dao.UserDao;
import com.example.mvvmrxroom.data.model.db.Option;
import com.example.mvvmrxroom.data.model.db.Question;
import com.example.mvvmrxroom.data.model.db.User;

@Database(entities = {User.class, Question.class, Option.class}, version = 2,exportSchema = false)
public abstract class AppDatabase extends RoomDatabase {

    public abstract OptionDao optionDao();

    public abstract QuestionDao questionDao();

    public abstract UserDao userDao();
}
