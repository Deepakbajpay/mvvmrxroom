package com.example.mvvmrxroom.di.component;

import android.app.Application;

import com.example.mvvmrxroom.MvvmRxRoomApp;
import com.example.mvvmrxroom.di.builder.ActivityBuilder;
import com.example.mvvmrxroom.di.module.AppModule;

import javax.inject.Singleton;

import dagger.BindsInstance;
import dagger.Component;
import dagger.android.AndroidInjectionModule;

@Singleton
@Component(modules = {AndroidInjectionModule.class, AppModule.class, ActivityBuilder.class})
public interface AppComponent {

    void inject(MvvmRxRoomApp app);

    @Component.Builder
    interface Builder {

        @BindsInstance
        Builder application(Application application);

        AppComponent build();
    }
}
